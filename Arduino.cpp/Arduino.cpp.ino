/**
* @ Arduino with Neopixel code
* @ v1.11
* 
* @ 전력 부족으로 중앙 4x4만 사용하는 코드
*/

#include <Adafruit_NeoPixel.h>

/************* 사용자 설정부 ******************/
#define BASE 10
#define PIN 6   
#define NUM_LEDS 64 
#define DEPTH 3
#define MAXARRSIZE (1+4+4+DEPTH+DEPTH-1)
#define DELAY_LENGTH 350

/************* 자료형 선언부 ******************/

enum epatterns {
  ZERO,
  ONE,
  TWO,
  TRHEE,
  FOUR,
  FIVE,
  SIX,
  SEVEN,
  EIGHT,
  NINE,
  ESEPAR,
  ESTART,
  PATTERN1,
  PATTERN2,
  MAXMODE = PATTERN2,
};

/************* 전역 변수 선언부 ****************/

Adafruit_NeoPixel neostrip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);

int ainfo[MAXARRSIZE];
int ainfo_size = 0;

int pixels[16] = { //data
                    27, 28, 29,
                    35, 36, 37,
                    43, 44, 45,
                    //dir
                    18, 19, 20, 21,
                    26, 
                    34, 
                    42  };

int patterns[12][9] = {  { 1, 1, 1, 
                0, 1, 0, // 패턴 #0
                0, 1, 0 },
                
              { 0, 0, 1, // 패턴 #1
                1, 1, 1,
                0, 0, 1 },

              { 0, 1, 0, // 패턴 #2
                0, 1, 0,
                1, 1, 1 },

              { 1, 0, 1, // 패턴 #3
                1, 0, 1,
                0, 1, 0 },

              { 0, 1, 1, // 패턴 #4
                1, 0, 0,
                0, 1, 1 },

              { 0, 1, 0, // 패턴 #5
                1, 0, 1,
                1, 0, 1 },

              { 0, 1, 1, // 패턴 #6
                1, 0, 0,
                1, 0, 1 },

              { 1, 0, 1, // 패턴 #7
                0, 0, 1,
                1, 1, 0 },

              { 1, 1, 0, // 패턴 #8
                0, 0, 1,
                1, 0, 1 },

              { 1, 0, 1, // 패턴 #9
                1, 0, 0,
                0, 1, 1 },

              { 1, 0, 1, // 구분 패턴
                0, 1, 0,
                1, 0, 1 },
                
              { 0, 1, 0, 
                1, 1, 1,  // 시작패턴
                0, 1, 0 }
            };

/************* 함수 정의부 ********************/

void ainsert(int val) {
  if(ainfo_size == MAXARRSIZE) {
    return;
  }
  // if rear is same as val
  else if(ainfo[ainfo_size - 1] == val) {
    ainfo[ainfo_size++] = ESEPAR;
    ainfo[ainfo_size++] = val;
    return;
  }
  else {
    ainfo[ainfo_size++] = val;
    return;
  }
}

void aclear() {
  ainfo_size = 0;
}

void setInfo(int _bn, int _fl, int _lo[DEPTH]) {
  aclear();
  ainsert(ESTART);
  ainsert(_bn / BASE);
  ainsert(_bn % BASE);
  ainsert(_fl / BASE);
  ainsert(_fl % BASE);
  for (int i = 0; i < DEPTH; i++)
    ainsert(_lo[i]);
}

void setInfo(epatterns index) {
  aclear();
  ainsert(index);
}

void setDirPixel() {
  neostrip.setPixelColor(pixels[9] , 150, 0, 0);
  neostrip.setPixelColor(pixels[10], 150, 150, 150);
  neostrip.setPixelColor(pixels[11], 150, 150, 150);
  neostrip.setPixelColor(pixels[12], 150, 0, 0);
  neostrip.setPixelColor(pixels[13], 150, 150, 150);
  neostrip.setPixelColor(pixels[14], 150, 150, 150);
  neostrip.setPixelColor(pixels[15], 150, 0, 0);
}

void setDataPixel(int index) {
  for (int i = 0; i < 9; i++) {
    neostrip.setPixelColor(pixels[i], patterns[index][i]);
  }
}

void showPattern(int index) {
  setDirPixel();
  setDataPixel(index);
  neostrip.show();
}

void ledLoop() {
  for (int i = 0; i < ainfo_size; i++) {
    showPattern(i);
    delay(DELAY_LENGTH);
  }
}

void setup() {
  //출력할 패턴(루프) 설정
  epatterns ep = PATTERN1;
  int lo[2][DEPTH] = { { 4, 5, 6 },
                        { 9, 9, 1 } };
  
  switch(ep) {
    case PATTERN1:
      setInfo(12, 3, lo[0]);
      break;
    case PATTERN2:
      setInfo(7, 8, lo[1]);
      break;
    default:
      setInfo(ep);
  }
  
  neostrip.begin();
}

void loop() {
  ledLoop();
}
